package com.example.mynotesapp.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.mynotesapp.R;
import com.example.mynotesapp.constant.AppConstant;
import com.example.mynotesapp.constant.PrefConstant;
import com.google.android.material.textfield.TextInputEditText;

public class LoginActivity extends AppCompatActivity {
    private TextInputEditText editTextFullName;
    private TextInputEditText editTextUserName;
    private Button buttonLogin;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        bindUi();
    }

    private void bindUi() {
        editTextFullName = findViewById(R.id.editTextUserName);
        editTextUserName = findViewById(R.id.editTextUserName);
        buttonLogin = findViewById(R.id.button);
        setUpSharedPreference();
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String fullName = editTextFullName.getText().toString();
                String userName = editTextUserName.getText().toString();
                if (!TextUtils.isEmpty(fullName) &&  !TextUtils.isEmpty(userName)) {

                    Intent intent = new Intent(LoginActivity.this, MyNotesActiity.class);
                    intent.putExtra(AppConstant.FULL_NAME, fullName);
                    startActivity(intent);
                    //login
                    saveLoginSatuts();
saveFullName(fullName);

                }else {
                    Toast.makeText(LoginActivity.this,"FullName is empty",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void saveFullName(String fullName) {
editor = sharedPreferences.edit();
editor.putString(PrefConstant.FULL_NAME,fullName);

    }


    private void saveLoginSatuts() {
        editor = sharedPreferences.edit();
        editor.putBoolean(PrefConstant.IS_lOGGED_IN,true);
        editor.apply();
    }

    private void setUpSharedPreference() {
        sharedPreferences = getSharedPreferences(PrefConstant.SHARED_PREFERENCE_NAME,MODE_PRIVATE);
    }
}